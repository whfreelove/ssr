{
  ["children"] = {
    ["SSR - Background"] = {
      ["actions"] = {
        ["finish"] = {
        },
        ["init"] = {
        },
        ["start"] = {
        },
      },
      ["adjustedMin"] = "1",
      ["alpha"] = 0.3,
      ["anchorFrameFrame"] = "WeakAuras:SSR - Bar",
      ["anchorFrameParent"] = false,
      ["anchorFrameType"] = "SELECTFRAME",
      ["anchorPoint"] = "TOP",
      ["animation"] = {
        ["finish"] = {
          ["alpha"] = 0,
          ["alphaFunc"] = "function(progress, start, delta)\\n    return start + (progress * delta)\\nend\\n",
          ["alphaType"] = "straight",
          ["colorA"] = 1,
          ["colorB"] = 1,
          ["colorG"] = 1,
          ["colorR"] = 1,
          ["duration_type"] = "seconds",
          ["easeStrength"] = 5,
          ["easeType"] = "easeOut",
          ["preset"] = "fade",
          ["rotate"] = 0,
          ["scalex"] = 1,
          ["scaley"] = 1,
          ["type"] = "none",
          ["use_alpha"] = false,
          ["x"] = 0,
          ["y"] = 0,
        },
        ["main"] = {
          ["duration_type"] = "seconds",
          ["easeStrength"] = 3,
          ["easeType"] = "none",
          ["type"] = "none",
        },
        ["start"] = {
          ["duration_type"] = "seconds",
          ["easeStrength"] = 3,
          ["easeType"] = "none",
          ["preset"] = "fade",
          ["type"] = "none",
        },
      },
      ["authorOptions"] = {
      },
      ["backgroundColor"] = {
        0, -- [1]
        0, -- [2]
        0, -- [3]
        0.5, -- [4]
      },
      ["barColor"] = {
        0, -- [1]
        0, -- [2]
        0, -- [3]
        1, -- [4]
      },
      ["conditions"] = {
      },
      ["config"] = {
      },
      ["desaturate"] = false,
      ["frameStrata"] = 2,
      ["height"] = 212,
      ["icon"] = false,
      ["iconSource"] = -1,
      ["icon_color"] = {
        1, -- [1]
        1, -- [2]
        1, -- [3]
        1, -- [4]
      },
      ["icon_side"] = "RIGHT",
      ["id"] = "SSR - Background",
      ["information"] = {
      },
      ["internalVersion"] = 45,
      ["inverse"] = true,
      ["load"] = {
        ["class"] = {
          ["multi"] = {
          },
        },
        ["size"] = {
          ["multi"] = {
          },
        },
        ["spec"] = {
          ["multi"] = {
          },
        },
        ["talent"] = {
          ["multi"] = {
          },
        },
        ["use_never"] = false,
      },
      ["orientation"] = "HORIZONTAL",
      ["parent"] = "Shred Shred Revolution",
      ["regionType"] = "aurabar",
      ["selfPoint"] = "BOTTOM",
      ["smoothProgress"] = false,
      ["spark"] = false,
      ["sparkBlendMode"] = "ADD",
      ["sparkColor"] = {
        1, -- [1]
        1, -- [2]
        1, -- [3]
        1, -- [4]
      },
      ["sparkHeight"] = 30,
      ["sparkHidden"] = "NEVER",
      ["sparkOffsetX"] = 0,
      ["sparkOffsetY"] = 0,
      ["sparkRotation"] = 0,
      ["sparkRotationMode"] = "AUTO",
      ["sparkTexture"] = "Interface\\CastingBar\\UI-CastingBar-Spark",
      ["sparkWidth"] = 10,
      ["subRegions"] = {
        {
          ["type"] = "aurabar_bar",
        }, -- [1]
      },
      ["texture"] = "ElvUI Norm",
      ["triggers"] = {
        {
          ["trigger"] = {
            ["check"] = "event",
            ["custom_hide"] = "custom",
            ["custom_type"] = "event",
            ["debuffType"] = "HELPFUL",
            ["event"] = "Stance/Form/Aura",
            ["events"] = "SSR_BAR_SHOW",
            ["form"] = {
              ["single"] = 3,
            },
            ["genericShowOn"] = "showOnCooldown",
            ["itemName"] = 0,
            ["names"] = {
            },
            ["spellIds"] = {
            },
            ["subeventPrefix"] = "SPELL",
            ["subeventSuffix"] = "_CAST_START",
            ["type"] = "unit",
            ["unit"] = "player",
            ["use_form"] = true,
            ["use_genericShowOn"] = true,
            ["use_itemName"] = true,
            ["use_unit"] = true,
          },
          ["untrigger"] = {
          },
        }, -- [1]
      },
      ["uid"] = "y2jb)0QFTtZ",
      ["useAdjustededMax"] = false,
      ["useAdjustededMin"] = false,
      ["width"] = 192,
      ["xOffset"] = 0,
      ["yOffset"] = 0,
      ["zoom"] = 0,
    },
    ["SSR - Bar"] = {
      ["actions"] = {
        ["finish"] = {
        },
        ["init"] = {
        },
        ["start"] = {
        },
      },
      ["adjustedMin"] = "1",
      ["alpha"] = 0.3,
      ["anchorFrameType"] = "SCREEN",
      ["anchorPoint"] = "CENTER",
      ["animation"] = {
        ["finish"] = {
          ["alpha"] = 0,
          ["alphaFunc"] = "function(progress, start, delta)\\n    return start + (progress * delta)\\nend\\n",
          ["alphaType"] = "straight",
          ["colorA"] = 1,
          ["colorB"] = 1,
          ["colorG"] = 1,
          ["colorR"] = 1,
          ["duration_type"] = "seconds",
          ["easeStrength"] = 5,
          ["easeType"] = "easeOut",
          ["preset"] = "fade",
          ["rotate"] = 0,
          ["scalex"] = 1,
          ["scaley"] = 1,
          ["type"] = "none",
          ["use_alpha"] = false,
          ["x"] = 0,
          ["y"] = 0,
        },
        ["main"] = {
          ["duration_type"] = "seconds",
          ["easeStrength"] = 3,
          ["easeType"] = "none",
          ["type"] = "none",
        },
        ["start"] = {
          ["duration_type"] = "seconds",
          ["easeStrength"] = 3,
          ["easeType"] = "none",
          ["type"] = "none",
        },
      },
      ["authorOptions"] = {
      },
      ["backgroundColor"] = {
        0, -- [1]
        0, -- [2]
        0, -- [3]
        0.5, -- [4]
      },
      ["barColor"] = {
        0.027450980392157, -- [1]
        0.69803921568627, -- [2]
        0.11372549019608, -- [3]
        1, -- [4]
      },
      ["conditions"] = {
      },
      ["config"] = {
      },
      ["desaturate"] = false,
      ["frameStrata"] = 2,
      ["height"] = 12,
      ["icon"] = false,
      ["iconSource"] = -1,
      ["icon_color"] = {
        1, -- [1]
        1, -- [2]
        1, -- [3]
        1, -- [4]
      },
      ["icon_side"] = "RIGHT",
      ["id"] = "SSR - Bar",
      ["information"] = {
      },
      ["internalVersion"] = 45,
      ["inverse"] = true,
      ["load"] = {
        ["class"] = {
          ["multi"] = {
          },
        },
        ["size"] = {
          ["multi"] = {
          },
        },
        ["spec"] = {
          ["multi"] = {
          },
        },
        ["talent"] = {
          ["multi"] = {
          },
        },
        ["use_never"] = false,
      },
      ["orientation"] = "HORIZONTAL",
      ["parent"] = "Shred Shred Revolution",
      ["regionType"] = "aurabar",
      ["selfPoint"] = "CENTER",
      ["smoothProgress"] = false,
      ["spark"] = false,
      ["sparkBlendMode"] = "ADD",
      ["sparkColor"] = {
        1, -- [1]
        1, -- [2]
        1, -- [3]
        1, -- [4]
      },
      ["sparkHeight"] = 30,
      ["sparkHidden"] = "EMPTY",
      ["sparkOffsetX"] = 0,
      ["sparkOffsetY"] = 0,
      ["sparkRotation"] = 0,
      ["sparkRotationMode"] = "AUTO",
      ["sparkTexture"] = "worldstate-capturebar-spark-yellow",
      ["sparkWidth"] = 10,
      ["subRegions"] = {
        {
          ["type"] = "aurabar_bar",
        }, -- [1]
      },
      ["texture"] = "ElvUI Norm",
      ["triggers"] = {
        {
          ["trigger"] = {
            ["check"] = "event",
            ["custom_hide"] = "custom",
            ["custom_type"] = "event",
            ["debuffType"] = "HELPFUL",
            ["event"] = "Stance/Form/Aura",
            ["events"] = "SSR_BAR_SHOW",
            ["form"] = {
              ["single"] = 3,
            },
            ["genericShowOn"] = "showOnCooldown",
            ["itemName"] = 0,
            ["names"] = {
            },
            ["spellIds"] = {
            },
            ["subeventPrefix"] = "SPELL",
            ["subeventSuffix"] = "_CAST_START",
            ["type"] = "unit",
            ["unit"] = "player",
            ["use_form"] = true,
            ["use_genericShowOn"] = true,
            ["use_itemName"] = true,
            ["use_unit"] = true,
          },
          ["untrigger"] = {
          },
        }, -- [1]
      },
      ["uid"] = "sa4)E8MRWn9",
      ["useAdjustededMax"] = false,
      ["useAdjustededMin"] = false,
      ["width"] = 192,
      ["xOffset"] = 0,
      ["yOffset"] = 0,
      ["zoom"] = 0,
    },
    ["SSR - ETick 1"] = {
      ["actions"] = {
        ["finish"] = {
        },
        ["init"] = {
        },
        ["start"] = {
        },
      },
      ["adjustedMax"] = ".4",
      ["adjustedMin"] = ".6",
      ["alpha"] = 1,
      ["anchorFrameFrame"] = "WeakAuras:SSR - Bar",
      ["anchorFrameParent"] = false,
      ["anchorFrameType"] = "SELECTFRAME",
      ["anchorPoint"] = "BOTTOM",
      ["animation"] = {
        ["finish"] = {
          ["alpha"] = 0,
          ["alphaFunc"] = "function(progress, start, delta)\\n    return start + (progress * delta)\\nend\\n",
          ["alphaType"] = "straight",
          ["colorA"] = 1,
          ["colorB"] = 1,
          ["colorG"] = 1,
          ["colorR"] = 1,
          ["duration_type"] = "seconds",
          ["easeStrength"] = 5,
          ["easeType"] = "easeOut",
          ["preset"] = "fade",
          ["rotate"] = 0,
          ["scalex"] = 1,
          ["scaley"] = 1,
          ["type"] = "none",
          ["use_alpha"] = false,
          ["x"] = 0,
          ["y"] = 0,
        },
        ["main"] = {
          ["duration_type"] = "seconds",
          ["easeStrength"] = 3,
          ["easeType"] = "none",
          ["type"] = "none",
        },
        ["start"] = {
          ["duration_type"] = "seconds",
          ["easeStrength"] = 3,
          ["easeType"] = "none",
          ["type"] = "none",
        },
      },
      ["authorOptions"] = {
      },
      ["backgroundColor"] = {
        0, -- [1]
        0, -- [2]
        0, -- [3]
        0, -- [4]
      },
      ["barColor"] = {
        0.98039215686275, -- [1]
        0.95294117647059, -- [2]
        0.090196078431373, -- [3]
        0, -- [4]
      },
      ["conditions"] = {
      },
      ["config"] = {
      },
      ["desaturate"] = false,
      ["frameStrata"] = 3,
      ["height"] = 64,
      ["icon"] = false,
      ["iconSource"] = -1,
      ["icon_color"] = {
        1, -- [1]
        1, -- [2]
        1, -- [3]
        1, -- [4]
      },
      ["icon_side"] = "LEFT",
      ["id"] = "SSR - ETick 1",
      ["information"] = {
      },
      ["internalVersion"] = 45,
      ["inverse"] = true,
      ["load"] = {
        ["class"] = {
          ["multi"] = {
          },
        },
        ["size"] = {
          ["multi"] = {
          },
        },
        ["spec"] = {
          ["multi"] = {
          },
        },
        ["talent"] = {
          ["multi"] = {
          },
        },
        ["use_never"] = false,
      },
      ["orientation"] = "VERTICAL",
      ["parent"] = "Shred Shred Revolution",
      ["regionType"] = "aurabar",
      ["selfPoint"] = "BOTTOM",
      ["smoothProgress"] = false,
      ["spark"] = true,
      ["sparkBlendMode"] = "ADD",
      ["sparkColor"] = {
        1, -- [1]
        1, -- [2]
        1, -- [3]
        1, -- [4]
      },
      ["sparkDesaturate"] = false,
      ["sparkHeight"] = 3,
      ["sparkHidden"] = "NEVER",
      ["sparkOffsetX"] = 0,
      ["sparkOffsetY"] = 0,
      ["sparkRotation"] = 0,
      ["sparkRotationMode"] = "AUTO",
      ["sparkTexture"] = "worldstate-capturebar-spark-yellow",
      ["sparkWidth"] = 192,
      ["subRegions"] = {
        {
          ["type"] = "aurabar_bar",
        }, -- [1]
        {
          ["anchorXOffset"] = 0,
          ["anchorYOffset"] = 0,
          ["rotateText"] = "NONE",
          ["text_anchorPoint"] = "SPARK",
          ["text_automaticWidth"] = "Auto",
          ["text_color"] = {
            1, -- [1]
            1, -- [2]
            1, -- [3]
            1, -- [4]
          },
          ["text_fixedWidth"] = 64,
          ["text_font"] = "Standard Text Font",
          ["text_fontSize"] = 12,
          ["text_fontType"] = "None",
          ["text_justify"] = "CENTER",
          ["text_selfPoint"] = "CENTER",
          ["text_shadowColor"] = {
            0, -- [1]
            0, -- [2]
            0, -- [3]
            1, -- [4]
          },
          ["text_shadowXOffset"] = 1,
          ["text_shadowYOffset"] = -1,
          ["text_text"] = "%p",
          ["text_text_format_n_format"] = "none",
          ["text_text_format_p_format"] = "timed",
          ["text_text_format_p_time_dynamic_threshold"] = 60,
          ["text_text_format_p_time_format"] = 0,
          ["text_text_format_p_time_precision"] = 1,
          ["text_visible"] = false,
          ["text_wordWrap"] = "WordWrap",
          ["type"] = "subtext",
        }, -- [2]
      },
      ["texture"] = "ElvUI Norm",
      ["triggers"] = {
        {
          ["trigger"] = {
            ["debuffType"] = "HELPFUL",
            ["event"] = "Stance/Form/Aura",
            ["form"] = {
              ["single"] = 3,
            },
            ["names"] = {
            },
            ["spellIds"] = {
            },
            ["subeventPrefix"] = "SPELL",
            ["subeventSuffix"] = "_CAST_START",
            ["type"] = "unit",
            ["unit"] = "player",
            ["use_form"] = true,
            ["use_unit"] = true,
          },
          ["untrigger"] = {
          },
        }, -- [1]
        {
          ["trigger"] = {
            ["check"] = "event",
            ["custom"] = "function(allstates, event, time)\\n    if event == \"SSR_ENERGYTICK\" then\\n        allstates[\"\"] = {\\n            changed = true,\\n            duration = 2,\\n            expirationTime = GetTime()+2,\\n            progressType = \"timed\",\\n            show = true\\n        }\\n        return true\\n    end\\nend",
            ["custom_hide"] = "timed",
            ["custom_type"] = "stateupdate",
            ["debuffType"] = "HELPFUL",
            ["duration"] = "2",
            ["dynamicDuration"] = false,
            ["event"] = "Combat Log",
            ["events"] = "SSR_ENERGYTICK",
            ["genericShowOn"] = "showOnCooldown",
            ["itemName"] = 0,
            ["names"] = {
            },
            ["spellIds"] = {
            },
            ["subeventPrefix"] = "SPELL",
            ["subeventSuffix"] = "_CAST_START",
            ["type"] = "custom",
            ["unit"] = "player",
            ["use_genericShowOn"] = true,
            ["use_itemName"] = true,
            ["use_unit"] = true,
          },
          ["untrigger"] = {
            ["custom"] = "function(event, show)\\n    return not show\\nend",
          },
        }, -- [2]
      },
      ["uid"] = "OxHEzh5f42h",
      ["useAdjustededMax"] = false,
      ["useAdjustededMin"] = false,
      ["useTooltip"] = false,
      ["width"] = 192,
      ["xOffset"] = 0,
      ["yOffset"] = 0,
      ["zoom"] = 0,
    },
    ["SSR - ETick 2"] = {
      ["actions"] = {
        ["finish"] = {
        },
        ["init"] = {
        },
        ["start"] = {
        },
      },
      ["adjustedMax"] = ".4",
      ["adjustedMin"] = ".6",
      ["alpha"] = 1,
      ["anchorFrameFrame"] = "WeakAuras:SSR - ETick 1",
      ["anchorFrameParent"] = true,
      ["anchorFrameType"] = "SELECTFRAME",
      ["anchorPoint"] = "TOP",
      ["animation"] = {
        ["finish"] = {
          ["alpha"] = 0,
          ["alphaFunc"] = "function(progress, start, delta)\\n    return start + (progress * delta)\\nend\\n",
          ["alphaType"] = "straight",
          ["colorA"] = 1,
          ["colorB"] = 1,
          ["colorG"] = 1,
          ["colorR"] = 1,
          ["duration_type"] = "seconds",
          ["easeStrength"] = 5,
          ["easeType"] = "easeOut",
          ["preset"] = "fade",
          ["rotate"] = 0,
          ["scalex"] = 1,
          ["scaley"] = 1,
          ["type"] = "none",
          ["use_alpha"] = false,
          ["x"] = 0,
          ["y"] = 0,
        },
        ["main"] = {
          ["duration_type"] = "seconds",
          ["easeStrength"] = 3,
          ["easeType"] = "none",
          ["type"] = "none",
        },
        ["start"] = {
          ["duration_type"] = "seconds",
          ["easeStrength"] = 3,
          ["easeType"] = "none",
          ["type"] = "none",
        },
      },
      ["authorOptions"] = {
      },
      ["backgroundColor"] = {
        0, -- [1]
        0, -- [2]
        0, -- [3]
        0, -- [4]
      },
      ["barColor"] = {
        0.98039215686275, -- [1]
        0.95294117647059, -- [2]
        0.090196078431373, -- [3]
        0, -- [4]
      },
      ["conditions"] = {
      },
      ["config"] = {
      },
      ["desaturate"] = false,
      ["frameStrata"] = 3,
      ["height"] = 64,
      ["icon"] = false,
      ["iconSource"] = -1,
      ["icon_color"] = {
        1, -- [1]
        1, -- [2]
        1, -- [3]
        1, -- [4]
      },
      ["icon_side"] = "LEFT",
      ["id"] = "SSR - ETick 2",
      ["information"] = {
      },
      ["internalVersion"] = 45,
      ["inverse"] = true,
      ["load"] = {
        ["class"] = {
          ["multi"] = {
          },
        },
        ["size"] = {
          ["multi"] = {
          },
        },
        ["spec"] = {
          ["multi"] = {
          },
        },
        ["talent"] = {
          ["multi"] = {
          },
        },
        ["use_never"] = false,
      },
      ["orientation"] = "VERTICAL",
      ["parent"] = "Shred Shred Revolution",
      ["regionType"] = "aurabar",
      ["selfPoint"] = "BOTTOM",
      ["smoothProgress"] = false,
      ["spark"] = true,
      ["sparkBlendMode"] = "ADD",
      ["sparkColor"] = {
        1, -- [1]
        1, -- [2]
        1, -- [3]
        1, -- [4]
      },
      ["sparkDesaturate"] = false,
      ["sparkHeight"] = 3,
      ["sparkHidden"] = "NEVER",
      ["sparkOffsetX"] = 0,
      ["sparkOffsetY"] = 0,
      ["sparkRotation"] = 0,
      ["sparkRotationMode"] = "AUTO",
      ["sparkTexture"] = "worldstate-capturebar-spark-yellow",
      ["sparkWidth"] = 192,
      ["subRegions"] = {
        {
          ["type"] = "aurabar_bar",
        }, -- [1]
      },
      ["texture"] = "ElvUI Norm",
      ["triggers"] = {
        {
          ["trigger"] = {
            ["debuffType"] = "HELPFUL",
            ["event"] = "Stance/Form/Aura",
            ["form"] = {
              ["single"] = 3,
            },
            ["names"] = {
            },
            ["spellIds"] = {
            },
            ["subeventPrefix"] = "SPELL",
            ["subeventSuffix"] = "_CAST_START",
            ["type"] = "unit",
            ["unit"] = "player",
            ["use_form"] = true,
            ["use_unit"] = true,
          },
          ["untrigger"] = {
          },
        }, -- [1]
        {
          ["trigger"] = {
            ["check"] = "event",
            ["custom"] = "function(allstates, event, time)\\n    if event == \"SSR_ENERGYTICK\" then\\n        allstates[\"\"] = {\\n            changed = true,\\n            duration = 2,\\n            expirationTime = GetTime()+2,\\n            progressType = \"timed\",\\n            show = true\\n        }\\n        return true\\n    end\\nend",
            ["custom_hide"] = "timed",
            ["custom_type"] = "stateupdate",
            ["debuffType"] = "HELPFUL",
            ["duration"] = "2",
            ["dynamicDuration"] = false,
            ["event"] = "Combat Log",
            ["events"] = "SSR_ENERGYTICK",
            ["genericShowOn"] = "showOnCooldown",
            ["itemName"] = 0,
            ["names"] = {
            },
            ["spellIds"] = {
            },
            ["subeventPrefix"] = "SPELL",
            ["subeventSuffix"] = "_CAST_START",
            ["type"] = "custom",
            ["unit"] = "player",
            ["use_genericShowOn"] = true,
            ["use_itemName"] = true,
            ["use_unit"] = true,
          },
          ["untrigger"] = {
            ["custom"] = "function(event, show)\\n    return not show\\nend",
          },
        }, -- [2]
      },
      ["uid"] = "BdteWE7hxIH",
      ["useAdjustededMax"] = false,
      ["useAdjustededMin"] = false,
      ["useTooltip"] = false,
      ["width"] = 192,
      ["xOffset"] = 0,
      ["yOffset"] = 0,
      ["zoom"] = 0,
    },
    ["SSR - ETick 3"] = {
      ["actions"] = {
        ["finish"] = {
        },
        ["init"] = {
        },
        ["start"] = {
        },
      },
      ["adjustedMax"] = ".4",
      ["adjustedMin"] = ".6",
      ["alpha"] = 1,
      ["anchorFrameFrame"] = "WeakAuras:SSR - ETick 2",
      ["anchorFrameParent"] = false,
      ["anchorFrameType"] = "SELECTFRAME",
      ["anchorPoint"] = "TOP",
      ["animation"] = {
        ["finish"] = {
          ["alpha"] = 0,
          ["alphaFunc"] = "function(progress, start, delta)\\n    return start + (progress * delta)\\nend\\n",
          ["alphaType"] = "straight",
          ["colorA"] = 1,
          ["colorB"] = 1,
          ["colorG"] = 1,
          ["colorR"] = 1,
          ["duration_type"] = "seconds",
          ["easeStrength"] = 5,
          ["easeType"] = "easeOut",
          ["preset"] = "fade",
          ["rotate"] = 0,
          ["scalex"] = 1,
          ["scaley"] = 1,
          ["type"] = "none",
          ["use_alpha"] = false,
          ["x"] = 0,
          ["y"] = 0,
        },
        ["main"] = {
          ["duration_type"] = "seconds",
          ["easeStrength"] = 3,
          ["easeType"] = "none",
          ["type"] = "none",
        },
        ["start"] = {
          ["duration_type"] = "seconds",
          ["easeStrength"] = 3,
          ["easeType"] = "none",
          ["type"] = "none",
        },
      },
      ["authorOptions"] = {
      },
      ["backgroundColor"] = {
        0, -- [1]
        0, -- [2]
        0, -- [3]
        0, -- [4]
      },
      ["barColor"] = {
        0.98039215686275, -- [1]
        0.95294117647059, -- [2]
        0.090196078431373, -- [3]
        0, -- [4]
      },
      ["conditions"] = {
      },
      ["config"] = {
      },
      ["desaturate"] = false,
      ["frameStrata"] = 3,
      ["height"] = 64,
      ["icon"] = false,
      ["iconSource"] = -1,
      ["icon_color"] = {
        1, -- [1]
        1, -- [2]
        1, -- [3]
        1, -- [4]
      },
      ["icon_side"] = "LEFT",
      ["id"] = "SSR - ETick 3",
      ["information"] = {
      },
      ["internalVersion"] = 45,
      ["inverse"] = true,
      ["load"] = {
        ["class"] = {
          ["multi"] = {
          },
        },
        ["size"] = {
          ["multi"] = {
          },
        },
        ["spec"] = {
          ["multi"] = {
          },
        },
        ["talent"] = {
          ["multi"] = {
          },
        },
        ["use_never"] = false,
      },
      ["orientation"] = "VERTICAL",
      ["parent"] = "Shred Shred Revolution",
      ["regionType"] = "aurabar",
      ["selfPoint"] = "BOTTOM",
      ["smoothProgress"] = false,
      ["spark"] = true,
      ["sparkBlendMode"] = "ADD",
      ["sparkColor"] = {
        1, -- [1]
        1, -- [2]
        1, -- [3]
        1, -- [4]
      },
      ["sparkDesaturate"] = false,
      ["sparkHeight"] = 3,
      ["sparkHidden"] = "NEVER",
      ["sparkOffsetX"] = 0,
      ["sparkOffsetY"] = 0,
      ["sparkRotation"] = 0,
      ["sparkRotationMode"] = "AUTO",
      ["sparkTexture"] = "worldstate-capturebar-spark-yellow",
      ["sparkWidth"] = 192,
      ["subRegions"] = {
        {
          ["type"] = "aurabar_bar",
        }, -- [1]
      },
      ["texture"] = "ElvUI Norm",
      ["triggers"] = {
        {
          ["trigger"] = {
            ["debuffType"] = "HELPFUL",
            ["event"] = "Stance/Form/Aura",
            ["form"] = {
              ["single"] = 3,
            },
            ["names"] = {
            },
            ["spellIds"] = {
            },
            ["subeventPrefix"] = "SPELL",
            ["subeventSuffix"] = "_CAST_START",
            ["type"] = "unit",
            ["unit"] = "player",
            ["use_form"] = true,
            ["use_unit"] = true,
          },
          ["untrigger"] = {
          },
        }, -- [1]
        {
          ["trigger"] = {
            ["check"] = "event",
            ["custom"] = "function(allstates, event, time)\\n    if event == \"SSR_ENERGYTICK\" then\\n        allstates[\"\"] = {\\n            changed = true,\\n            duration = 2,\\n            expirationTime = GetTime()+2,\\n            progressType = \"timed\",\\n            show = true\\n        }\\n        return true\\n    end\\nend",
            ["custom_hide"] = "timed",
            ["custom_type"] = "stateupdate",
            ["debuffType"] = "HELPFUL",
            ["duration"] = "2",
            ["dynamicDuration"] = false,
            ["event"] = "Combat Log",
            ["events"] = "SSR_ENERGYTICK",
            ["genericShowOn"] = "showOnCooldown",
            ["itemName"] = 0,
            ["names"] = {
            },
            ["spellIds"] = {
            },
            ["subeventPrefix"] = "SPELL",
            ["subeventSuffix"] = "_CAST_START",
            ["type"] = "custom",
            ["unit"] = "player",
            ["use_genericShowOn"] = true,
            ["use_itemName"] = true,
            ["use_unit"] = true,
          },
          ["untrigger"] = {
            ["custom"] = "function(event, show)\\n    return not show\\nend",
          },
        }, -- [2]
      },
      ["uid"] = "KayWLPo4Ocw",
      ["useAdjustededMax"] = false,
      ["useAdjustededMin"] = false,
      ["useTooltip"] = false,
      ["width"] = 192,
      ["xOffset"] = 0,
      ["yOffset"] = 0,
      ["zoom"] = 0,
    },
    ["SSR - GCD"] = {
      ["actions"] = {
        ["finish"] = {
        },
        ["init"] = {
        },
        ["start"] = {
        },
      },
      ["adjustedMax"] = ".4",
      ["adjustedMin"] = ".6",
      ["alpha"] = 1,
      ["anchorFrameFrame"] = "WeakAuras:SSR - Bar",
      ["anchorFrameParent"] = false,
      ["anchorFrameType"] = "SELECTFRAME",
      ["anchorPoint"] = "BOTTOM",
      ["animation"] = {
        ["finish"] = {
          ["alpha"] = 0,
          ["alphaFunc"] = "function(progress, start, delta)\\n    return start + (progress * delta)\\nend\\n",
          ["alphaType"] = "straight",
          ["colorA"] = 1,
          ["colorB"] = 1,
          ["colorG"] = 1,
          ["colorR"] = 1,
          ["duration_type"] = "seconds",
          ["easeStrength"] = 5,
          ["easeType"] = "easeOut",
          ["preset"] = "fade",
          ["rotate"] = 0,
          ["scalex"] = 1,
          ["scaley"] = 1,
          ["type"] = "none",
          ["use_alpha"] = false,
          ["x"] = 0,
          ["y"] = 0,
        },
        ["main"] = {
          ["duration_type"] = "seconds",
          ["easeStrength"] = 3,
          ["easeType"] = "none",
          ["type"] = "none",
        },
        ["start"] = {
          ["duration_type"] = "seconds",
          ["easeStrength"] = 3,
          ["easeType"] = "none",
          ["type"] = "none",
        },
      },
      ["authorOptions"] = {
      },
      ["backgroundColor"] = {
        0, -- [1]
        0, -- [2]
        0, -- [3]
        0, -- [4]
      },
      ["barColor"] = {
        0, -- [1]
        0, -- [2]
        0, -- [3]
        0.45523810386658, -- [4]
      },
      ["conditions"] = {
      },
      ["config"] = {
      },
      ["desaturate"] = false,
      ["frameStrata"] = 4,
      ["height"] = 48,
      ["icon"] = false,
      ["iconSource"] = -1,
      ["icon_color"] = {
        1, -- [1]
        1, -- [2]
        1, -- [3]
        1, -- [4]
      },
      ["icon_side"] = "RIGHT",
      ["id"] = "SSR - GCD",
      ["information"] = {
      },
      ["internalVersion"] = 45,
      ["inverse"] = false,
      ["load"] = {
        ["class"] = {
          ["multi"] = {
          },
        },
        ["size"] = {
          ["multi"] = {
          },
        },
        ["spec"] = {
          ["multi"] = {
          },
        },
        ["talent"] = {
          ["multi"] = {
          },
        },
        ["use_never"] = false,
      },
      ["orientation"] = "VERTICAL_INVERSE",
      ["parent"] = "Shred Shred Revolution",
      ["regionType"] = "aurabar",
      ["selfPoint"] = "BOTTOM",
      ["smoothProgress"] = false,
      ["spark"] = true,
      ["sparkBlendMode"] = "ADD",
      ["sparkColor"] = {
        1, -- [1]
        1, -- [2]
        1, -- [3]
        1, -- [4]
      },
      ["sparkDesaturate"] = true,
      ["sparkHeight"] = 3,
      ["sparkHidden"] = "NEVER",
      ["sparkOffsetX"] = 0,
      ["sparkOffsetY"] = 0,
      ["sparkRotation"] = 0,
      ["sparkRotationMode"] = "AUTO",
      ["sparkTexture"] = "worldstate-capturebar-spark-yellow",
      ["sparkWidth"] = 192,
      ["subRegions"] = {
        {
          ["type"] = "aurabar_bar",
        }, -- [1]
      },
      ["texture"] = "ElvUI Norm",
      ["triggers"] = {
        {
          ["trigger"] = {
            ["debuffType"] = "HELPFUL",
            ["event"] = "Stance/Form/Aura",
            ["form"] = {
              ["single"] = 3,
            },
            ["names"] = {
            },
            ["spellIds"] = {
            },
            ["subeventPrefix"] = "SPELL",
            ["subeventSuffix"] = "_CAST_START",
            ["type"] = "unit",
            ["unit"] = "player",
            ["use_form"] = true,
            ["use_unit"] = true,
          },
          ["untrigger"] = {
          },
        }, -- [1]
        {
          ["trigger"] = {
            ["check"] = "event",
            ["custom_hide"] = "timed",
            ["custom_type"] = "stateupdate",
            ["debuffType"] = "HELPFUL",
            ["duration"] = "2",
            ["dynamicDuration"] = false,
            ["event"] = "Global Cooldown",
            ["events"] = "SSR_ENERGYTICK",
            ["genericShowOn"] = "showOnCooldown",
            ["itemName"] = 0,
            ["names"] = {
            },
            ["realSpellName"] = 0,
            ["spellIds"] = {
            },
            ["spellName"] = 0,
            ["subeventPrefix"] = "SPELL",
            ["subeventSuffix"] = "_CAST_START",
            ["type"] = "spell",
            ["unit"] = "player",
            ["use_genericShowOn"] = true,
            ["use_itemName"] = true,
            ["use_spellName"] = true,
            ["use_track"] = true,
            ["use_unit"] = true,
          },
          ["untrigger"] = {
          },
        }, -- [2]
      },
      ["uid"] = "EvubcziP8h7",
      ["useAdjustededMax"] = false,
      ["useAdjustededMin"] = false,
      ["useTooltip"] = false,
      ["width"] = 192,
      ["xOffset"] = 0,
      ["yOffset"] = 0,
      ["zoom"] = 0,
    },
    ["SSR - Main"] = {
      ["actions"] = {
        ["finish"] = {
        },
        ["init"] = {
          ["custom"] = "if aura_env.config.log <= 2 then\\n    print(\"\")\\n    print(\"============Init============\")\\nend\\naura_env.barName = \"SSR - Bar\"\\naura_env.barHeight = WeakAuras.GetRegion(aura_env.barName).height\\naura_env.etick = {}\\naura_env.suggestion = {}\\naura_env.suggestionState = {}\\n\\nlocal function dumpTable(tbl, fmt)\\n    if not fmt then fmt = tostring end\\n    local res = \"\"\\n    for k,v in pairs(tbl) do\\n        res = res..\" \"..k..\"=\"..fmt(v)\\n    end\\n    return res\\nend\\n\\nlocal function fmtTime(t)\\n    return math.floor(t%60*100)/100\\nend\\n\\nlocal function cooldownExpTime(spellID)\\n    local start,duration,_,_ = GetSpellCooldown(spellID)\\n    if start == 0 then\\n        return\\n    else\\n        return start + duration\\n    end\\nend\\n\\nlocal function searchAura(unit, filter, sourcePlayer, searchIDs)\\n    for i=1,40 do\\n        local _,_,_,_,_,expTime,source,_,_,spellID = UnitAura(unit, i, filter)\\n        if not spellID or (sourcePlayer and source ~= \"player\") then\\n            break\\n        end\\n        for _,searchID in ipairs(searchIDs) do\\n            if aura_env.config.log <= 1 then\\n                print(\\n                    unit..\\n                    \" spell=\"..spellID..\\n                    \"|search=\"..searchID..\\n                    \" source=\"..source\\n                )\\n            end\\n            if spellID == searchID then\\n                return true, expTime\\n            end\\n        end\\n    end\\n    return false, GetTime()-1\\nend\\n\\nlocal function stateMatch(simState)\\n    for k in pairs(simState) do\\n        if simState[k] ~= aura_env.suggestionState[k] then\\n            return false\\n        end\\n    end\\n    return true\\nend\\n\\nlocal function getState()\\n    local time = GetTime()\\n    local autoattack = IsCurrentSpell(6603)\\n    local energy = UnitPower(\"player\", 3)\\n    local comboPts = GetComboPoints(\"player\", \"target\")\\n    local mana = UnitPower(\"player\", 0)\\n    local faerieCDExpTime = cooldownExpTime(27011)\\n    local gcdExpTime = cooldownExpTime(29515)\\n    local cc, ccExpTime = searchAura(\"player\", \"HELPFUL\", false, {16870})\\n    local faerie, faerieExpTime = searchAura(\"target\", \"HARMFUL\", false, {26993, 27011})\\n    local mangle, mangleExpTime = searchAura(\"target\", \"HARMFUL\", false, {33983, 33987})\\n    local rake, rakeExpTime = searchAura(\"target\", \"HARMFUL\", true, {27003})\\n    local rip, ripExpTime = searchAura(\"target\", \"HARMFUL\", true, {27008})\\n    local simState = {\\n        time = 0,\\n        autoattack = autoattack,\\n        energy = energy,\\n        comboPts = comboPts,\\n        mana = mana,\\n        gcd = not not gcdExpTime,\\n        cc = cc,\\n        faerie = faerie,\\n        mangle = mangle,\\n        rake = rake,\\n        rip = rip,\\n        etickExpTime = ((aura_env.etick.time or 0)-time)%2+time,\\n        faerieCDExpTime = faerieCDExpTime or time-1,\\n        gcdExpTime = gcdExpTime or time-1,\\n        ccExpTime = ccExpTime,\\n        faerieExpTime = faerieExpTime,\\n        mangleExpTime = mangleExpTime,\\n        rakeExpTime = rakeExpTime,\\n        ripExpTime = ripExpTime,\\n    }\\n    if aura_env.config.log <= 2 then print(\"simState: \"..aura_env.func.dumpTable(simState)) end\\n    return simState\\nend\\n\\nlocal function suggest(simState)\\n    local spellInfo = function(spellID)\\n        local name,_,icon,_,_,_ = GetSpellInfo(spellID)\\n        return  {\\n            spellID = spellID,\\n            name = name,\\n            icon = icon,\\n        }\\n    end\\n\\n    local spellInfoCache = {\\n        [768] = spellInfo(768),  -- Cat Form\\n        [24248] = spellInfo(24248),  -- Ferocious Bite\\n        [27002] = spellInfo(27002),  -- Shred\\n        [27003] = spellInfo(27003),  -- Rake\\n        [27008] = spellInfo(27008),  -- Rip\\n        [27011] = spellInfo(27011),  -- Faerie Fire (Feral)\\n        [33983] = spellInfo(33983),  -- Mangle (Cat)\\n    }\\n\\n    -- Shallow copy of spell info so suggestions are independent\\n    local function spell(spellID)\\n        local res = {}\\n        for k,v in pairs(spellInfoCache[spellID]) do res[k] = v end\\n        return res\\n    end\\n\\n    local time = GetTime()\\n    \\n    -- Rotation\\n    if simState.gcdExpTime-time-simState.time > 0 then\\n        if aura_env.config.log <= 1 then print(\"GCD up: \"..aura_env.func.fmtTime(simState.gcdExpTime - time)) end\\n        return nil\\n    elseif not simState.cc and (simState.energy < 10 or (simState.energy < 20 and (simState.comboPts < 4 or simState.rip))) and simState.etickExpTime-time-simState.time > 0.5 then\\n        if aura_env.config.log <= 1 then print(\"Shift\") end\\n        suggest = spell(768)\\n    elseif not simState.rip and simState.comboPts >= 4 and (simState.energy >= 30 or simState.cc) then\\n        if aura_env.config.log <= 1 then print(\"Rip\") end\\n        suggest = spell(27008)\\n    elseif simState.mangle and (simState.energy >= 42 or simState.cc) then\\n        if aura_env.config.log <= 1 then print(\"Shred\") end\\n        suggest = spell(27002)\\n    elseif (simState.energy >= 40 or simState.cc) and (not simState.mangle or (simState.mangle and simState.energy < 42 and simState.etickExpTime-time-simState.time > 1)) then\\n        if aura_env.config.log <= 1 then print(\"Mangle\") end\\n        suggest = spell(33983)\\n    elseif simState.energy >= 35 and simState.mangle and simState.comboPts >= 2 and not simState.cc and simState.etickExpTime-time-simState.time > 1 then\\n        if aura_env.config.log <= 1 then print(\"Bite\") end\\n        suggest = spell(24248)\\n    elseif simState.energy >= 35 and simState.mangle and not simState.cc and simState.etickExpTime-time-simState.time > 1 and not simState.rake then\\n        if aura_env.config.log <= 1 then print(\"Rake\") end\\n        suggest = spell(27003)\\n    -- elseif simState.faerieCDExpTime-time-simState.time < 0 and (((not simState.faerie or simState.faerieExpTime-time-simState.time < 12) and simState.etickExpTime-time-simState.time > 1) or simState.etickExpTime-time-simState.time > 1.5) then  -- Prevent Faerie Fire from falling off\\n    --     if aura_env.config.log <= 1 then print(\"Faerie Fire\") end\\n    --     suggest = spell(27011)\\n    else\\n        if aura_env.config.log <= 1 then print(\"Wait\") end\\n        return nil\\n    end\\n\\n    suggest.autoHide = false\\n    suggest.changed = true\\n    suggest.duration = simState.time\\n    suggest.expirationTime = time+simState.time\\n    suggest.paused = false\\n    suggest.progressType = \"timed\"\\n    suggest.show = true\\n    return suggest\\nend\\n\\nlocal updateState = function(simState, suggestion)\\n    local time = GetTime()\\n    local remaining = {\\n        [0] = simState.etickExpTime-time,\\n        [16870] = simState.ccExpTime-time,\\n        [27003] = simState.rakeExpTime-time,\\n        [27008] = simState.ripExpTime-time,\\n        [27011] = simState.faerieExpTime-time,\\n        [29515] = simState.gcdExpTime-time,\\n        [33983] = simState.mangleExpTime-time,\\n    }\\n    if suggestion then\\n        -- NOTE: Assumes each suggestion triggers GCD 1.5s\\n        simState.gcdExpTime = time+simState.time+1.5\\n        remaining[29515] = simState.time+1.5\\n    end\\n    if aura_env.config.log <= 1 then print(\"  1 Remain:\"..dumpTable(remaining, aura_env.func.fmtTime)) end\\n    local updateCause = {\\n        -- Energy Tick [Fake ID]\\n        [0] = function(simState)\\n            simState.energy = simState.energy+20\\n            simState.etickExpTime = simState.etickExpTime+2\\n            return simState\\n        end,\\n        -- Clearcasting [Omen of Clarity]\\n        [16870] = function(simState)\\n            simState.cc = false\\n            simState.ccExpTime = GetTime()-1\\n            return simState\\n        end,\\n        -- Rake\\n        [27003] = function(simState)\\n            simState.rake = false\\n            simState.rakeExpTime = GetTime()-1\\n            return simState\\n        end,\\n        -- Rip\\n        [27008] = function(simState)\\n            simState.rip = false\\n            simState.ripExpTime = GetTime()-1\\n            return simState\\n        end,\\n        -- Faerie Fire (Feral)\\n        [27011] = function(simState)\\n            simState.faerie = false\\n            simState.faerieExpTime = GetTime()-1\\n            return simState\\n        end,\\n        -- GCD\\n        [29515] = function(simState)\\n            simState.gcdExpTime = GetTime()-1\\n            return simState\\n        end,\\n        -- Mangle (Cat)\\n        [33983] = function(simState)\\n            simState.mangle = false\\n            simState.mangleExpTime = GetTime()-1\\n            return simState\\n        end,\\n    }\\n    local spellEffect = {\\n        -- Powershift\\n        [768] = function(simState)\\n            -- TODO: Check for Wolfshead Helm\\n            simState.energy = 60\\n            return simState\\n        end,\\n        -- Ferocious Bite\\n        [24248] = function(simState)\\n            if simState.cc then\\n                simState.cc = false\\n            else\\n                simState.energy = 0\\n            end\\n            simState.comboPts = 0\\n            return simState\\n        end,\\n        -- Shred\\n        [27002] = function(simState)\\n            -- TODO: Check for talents and set bonuses?\\n            if simState.cc then\\n                simState.cc = false\\n            else\\n                simState.energy = simState.energy-42\\n            end\\n            simState.comboPts = simState.comboPts+1\\n            return simState\\n        end,\\n        -- Rake\\n        [27003] = function(simState)\\n            simState.energy = simState.energy-35\\n            simState.comboPts = simState.comboPts+1\\n            simState.rake = true\\n            simState.rakeExpTime = time+simState.time+9\\n            return simState\\n        end,\\n        -- Rip\\n        [27008] = function(simState)\\n            if simState.cc then\\n                simState.cc = false\\n            else\\n                simState.energy = simState.energy-30\\n            end\\n            simState.comboPts = 0\\n            simState.rip = true\\n            simState.ripExpTime = time+simState.time+12\\n            return simState\\n        end,\\n        -- Faerie Fire (Feral)\\n        [27011] = function(simState)\\n            simState.faerie = true\\n            simState.faerieExpTime = time+simState.time+40\\n            simState.faerieCDExpTime = time+simState.time+6\\n            return simState\\n        end,\\n        -- Mangle (Cat)\\n        [33983] = function(simState)\\n            -- TODO: Check for talents and set bonuses?\\n            if simState.cc then\\n                simState.cc = false\\n            else\\n                simState.energy = simState.energy-40\\n            end\\n            simState.comboPts = simState.comboPts+1\\n            simState.mangle = true\\n            simState.mangleExpTime = time+simState.time+12\\n            return simState\\n        end,\\n    }\\n    local idxNext\\n    local simTime = aura_env.config.maxSimTime\\n    for k,v in pairs(remaining) do\\n        if v >= simState.time and v < simTime then\\n            idxNext, simTime = k,v\\n        end\\n    end\\n    if not idxNext then\\n        if aura_env.config.log <= 1 then print(\"Simulation time limit is \"..aura_env.func.fmtTime(aura_env.config.maxSimTime)) end\\n        return\\n    end\\n    if aura_env.config.log <= 1 then print(\"  2 r=\"..aura_env.func.fmtTime(simTime)..\" from \"..idxNext) end\\n    simState = updateCause[idxNext](simState)\\n    if suggestion and spellEffect[suggestion.spellID] then\\n        if aura_env.config.log <= 1 then print(\"  3 sugSpellID=\"..suggestion.spellID) end\\n        simState = spellEffect[suggestion.spellID](simState)\\n    end\\n    simState.time = simTime\\n    if aura_env.config.log <= 1 then print(\"  4 next t=\"..aura_env.func.fmtTime(simState.time)) end\\n        return simState\\nend\\n\\naura_env.func = {\\n    dumpTable = dumpTable,\\n    fmtTime = fmtTime,\\n    getState = getState,\\n    stateMatch = stateMatch,\\n    suggest = suggest,\\n    updateState = updateState,\\n}\\n\\nWeakAuras.ScanEvents ('SSR_UPDATE')\\nif aura_env.config.log <= 2 then print(\"==========Init End==========\") end",
          ["do_custom"] = true,
        },
        ["start"] = {
        },
      },
      ["alpha"] = 0.6,
      ["anchorFrameFrame"] = "WeakAuras:SSR - Bar",
      ["anchorFrameParent"] = false,
      ["anchorFrameType"] = "SELECTFRAME",
      ["anchorPoint"] = "BOTTOMLEFT",
      ["animation"] = {
        ["finish"] = {
          ["alpha"] = 0,
          ["alphaFunc"] = "function(progress, start, delta)\\n    return start + (progress * delta)\\nend\\n",
          ["alphaType"] = "straight",
          ["colorA"] = 1,
          ["colorB"] = 1,
          ["colorG"] = 1,
          ["colorR"] = 1,
          ["duration_type"] = "seconds",
          ["easeStrength"] = 5,
          ["easeType"] = "easeOut",
          ["preset"] = "fade",
          ["rotate"] = 0,
          ["scalex"] = 1,
          ["scaley"] = 1,
          ["translateFunc"] = "\\n\\n",
          ["translateType"] = "custom",
          ["type"] = "none",
          ["use_alpha"] = false,
          ["use_translate"] = false,
          ["x"] = 0,
          ["y"] = 0,
        },
        ["main"] = {
          ["alpha"] = 0,
          ["alphaFunc"] = "    function(progress, start, delta)\\n      return start + (progress * delta)\\n    end\\n  ",
          ["alphaType"] = "custom",
          ["colorA"] = 1,
          ["colorB"] = 1,
          ["colorFunc"] = "",
          ["colorG"] = 1,
          ["colorR"] = 1,
          ["colorType"] = "custom",
          ["duration"] = "",
          ["duration_type"] = "seconds",
          ["easeStrength"] = 3,
          ["easeType"] = "none",
          ["rotate"] = 0,
          ["scaleFunc"] = "-- We are scaling start here, because of reuse of TSU happens when a spellcooldown is prolonged, such as in case of trinket switch\\nfunction ( _, startX, startY, scaleX, scaleY )\\n  if not aura_env.state.updated then return 1,1 end\\n  \\n  local progress = GetTime () - aura_env.state.updated\\n  if progress > 0.5 then return 1,1 end\\n  \\n  progress = math.min ( progress / 0.5, 1 )\\n  \\n  local size = progress * 2;\\n  size = 1 + (2 - size)\\n  \\n  return size,size\\nend",
          ["scaleType"] = "custom",
          ["scalex"] = 1,
          ["scaley"] = 1,
          ["translateFunc"] = "function(progress, startX, startY, deltaX, deltaY)\\n    if not aura_env.state then return end\\n    local time = GetTime()\\n    local expRel = aura_env.state.expirationTime - time\\n    local remaining = math.max(\\n        0,\\n        expRel,\\n        aura_env.state.anchorExpRel-aura_env.config.slackAnimation\\n    )\\n    if expRel <= aura_env.state.anchorExpRel then\\n        aura_env.state.paused = true\\n        aura_env.state.remaining = aura_env.state.anchorExpRel\\n    end\\n    if aura_env.config.log <= 0 then print(\"AniR: \"..aura_env.func.fmtTime(remaining)) end\\n    return 0, 196*remaining/aura_env.config.maxSimTime\\nend",
          ["translateType"] = "custom",
          ["type"] = "custom",
          ["use_alpha"] = false,
          ["use_color"] = false,
          ["use_scale"] = false,
          ["use_translate"] = true,
          ["x"] = 0,
          ["y"] = 0,
        },
        ["start"] = {
          ["alphaType"] = "custom",
          ["duration_type"] = "seconds",
          ["easeStrength"] = 3,
          ["easeType"] = "none",
          ["preset"] = "fade",
          ["type"] = "none",
          ["use_alpha"] = false,
          ["use_scale"] = true,
        },
      },
      ["authorOptions"] = {
        {
          ["fontSize"] = "medium",
          ["text"] = "Higher value makes it easier to see difference between cooldown as they are getting ready, lower values makes it easier to see long cooldowns",
          ["type"] = "description",
          ["width"] = 2,
        }, -- [1]
        {
          ["default"] = 0.3,
          ["desc"] = "Higher value makes it easier to see difference between cooldown as they are getting ready, lower values makes it easier to see long cooldowns",
          ["key"] = "multiplier",
          ["max"] = 2,
          ["min"] = 0,
          ["name"] = "Log multiplier",
          ["step"] = 0.05,
          ["type"] = "range",
          ["useDesc"] = false,
          ["width"] = 2,
        }, -- [2]
        {
          ["noMerge"] = false,
          ["text"] = "",
          ["type"] = "header",
          ["useName"] = false,
          ["width"] = 1,
        }, -- [3]
        {
          ["default"] = true,
          ["desc"] = "Should the bar hide itself when there's no cooldown being tracked",
          ["key"] = "autoHide",
          ["name"] = "Auto Hide",
          ["type"] = "toggle",
          ["useDesc"] = true,
          ["width"] = 1,
        }, -- [4]
        {
          ["default"] = 1,
          ["key"] = "untilHide",
          ["max"] = 5,
          ["min"] = 0,
          ["name"] = "Visible before hide",
          ["step"] = 0.25,
          ["type"] = "range",
          ["useDesc"] = false,
          ["width"] = 1,
        }, -- [5]
        {
          ["default"] = 6,
          ["key"] = "maxSimTime",
          ["max"] = 12,
          ["min"] = 0,
          ["name"] = "Simulation Time",
          ["step"] = 0.5,
          ["type"] = "range",
          ["useDesc"] = false,
          ["width"] = 1,
        }, -- [6]
        {
          ["bigStep"] = 1,
          ["default"] = 5,
          ["key"] = "log",
          ["max"] = 5,
          ["min"] = 0,
          ["name"] = "Logging Level",
          ["softMax"] = 5,
          ["softMin"] = 1,
          ["step"] = 0.5,
          ["type"] = "range",
          ["useDesc"] = false,
          ["width"] = 1,
        }, -- [7]
        {
          ["default"] = 0.5,
          ["key"] = "slackAnimation",
          ["max"] = 1.5,
          ["min"] = 0,
          ["name"] = "Icon Time Slack",
          ["step"] = 0.05,
          ["type"] = "number",
          ["useDesc"] = false,
          ["width"] = 0.3,
        }, -- [8]
      },
      ["auto"] = true,
      ["color"] = {
        1, -- [1]
        1, -- [2]
        1, -- [3]
        1, -- [4]
      },
      ["conditions"] = {
        {
          ["changes"] = {
            {
              ["property"] = "xOffsetRelative",
              ["value"] = 32,
            }, -- [1]
          },
          ["check"] = {
            ["op"] = "==",
            ["trigger"] = 1,
            ["value"] = "33983",
            ["variable"] = "spellID",
          },
        }, -- [1]
        {
          ["changes"] = {
            {
              ["property"] = "xOffsetRelative",
              ["value"] = 64,
            }, -- [1]
          },
          ["check"] = {
            ["op"] = "==",
            ["trigger"] = 1,
            ["value"] = "27008",
            ["variable"] = "spellID",
          },
        }, -- [2]
        {
          ["changes"] = {
            {
              ["property"] = "xOffsetRelative",
              ["value"] = 96,
            }, -- [1]
          },
          ["check"] = {
            ["op"] = "==",
            ["trigger"] = 1,
            ["value"] = "768",
            ["variable"] = "spellID",
          },
        }, -- [3]
        {
          ["changes"] = {
            {
              ["property"] = "xOffsetRelative",
              ["value"] = 128,
            }, -- [1]
          },
          ["check"] = {
            ["op"] = "==",
            ["trigger"] = 1,
            ["value"] = "24248",
            ["variable"] = "spellID",
          },
        }, -- [4]
        {
          ["changes"] = {
            {
              ["property"] = "xOffsetRelative",
              ["value"] = 160,
            }, -- [1]
          },
          ["check"] = {
            ["op"] = "==",
            ["trigger"] = 1,
            ["value"] = "27003",
            ["variable"] = "spellID",
          },
        }, -- [5]
        {
          ["changes"] = {
            {
              ["property"] = "xOffsetRelative",
              ["value"] = -32,
            }, -- [1]
          },
          ["check"] = {
            ["op"] = "==",
            ["trigger"] = 1,
            ["value"] = "27011",
            ["variable"] = "spellID",
          },
        }, -- [6]
      },
      ["config"] = {
        ["autoHide"] = false,
        ["log"] = 5,
        ["maxSimTime"] = 6,
        ["multiplier"] = 0.05,
        ["slackAnimation"] = 0,
        ["untilHide"] = 0.5,
      },
      ["cooldown"] = false,
      ["cooldownEdge"] = false,
      ["cooldownSwipe"] = true,
      ["cooldownTextDisabled"] = false,
      ["customText"] = "",
      ["desaturate"] = false,
      ["frameStrata"] = 5,
      ["height"] = 32,
      ["icon"] = true,
      ["iconSource"] = -1,
      ["id"] = "SSR - Main",
      ["information"] = {
        ["ignoreOptionsEventErrors"] = true,
      },
      ["internalVersion"] = 45,
      ["inverse"] = false,
      ["keepAspectRatio"] = false,
      ["load"] = {
        ["affixes"] = {
          ["multi"] = {
          },
        },
        ["class"] = {
          ["multi"] = {
          },
        },
        ["class_and_spec"] = {
          ["multi"] = {
          },
        },
        ["difficulty"] = {
          ["multi"] = {
          },
        },
        ["faction"] = {
          ["multi"] = {
          },
        },
        ["ingroup"] = {
          ["multi"] = {
          },
        },
        ["pvptalent"] = {
          ["multi"] = {
          },
        },
        ["race"] = {
          ["multi"] = {
          },
        },
        ["role"] = {
          ["multi"] = {
          },
        },
        ["size"] = {
          ["multi"] = {
          },
        },
        ["spec"] = {
          ["multi"] = {
          },
        },
        ["talent"] = {
          ["multi"] = {
          },
        },
        ["talent2"] = {
          ["multi"] = {
          },
        },
        ["talent3"] = {
          ["multi"] = {
          },
        },
        ["use_never"] = false,
        ["zoneIds"] = "",
      },
      ["parent"] = "Shred Shred Revolution",
      ["preferToUpdate"] = false,
      ["regionType"] = "icon",
      ["selfPoint"] = "BOTTOMLEFT",
      ["semver"] = "2.0.1",
      ["subRegions"] = {
      },
      ["tocversion"] = 11307,
      ["triggers"] = {
        {
          ["trigger"] = {
            ["check"] = "event",
            ["custom"] = "function(allstates, event)\\n    if not aura_env.func or GetShapeshiftFormID() ~= CAT_FORM then return false end\\n    if aura_env.config.log <= 2 then\\n        print(\"\")\\n        print(\"============Main============\")\\n    end\\n    local time = GetTime()\\n    local prevSug = next(aura_env.suggestion)\\n    -- If a previous suggestion already exists and this\\n    --     is in the same frame, do not update\\n    if prevSug and aura_env.suggestion.time == time then\\n        return false\\n    end\\n    \\n    local simState = aura_env.func.getState()\\n    -- If the state is unchanged, do not update\\n    if prevSug and aura_env.func.stateMatch(simState) then\\n        return false\\n    end\\n    -- Since the state is new, update the stored state\\n    aura_env.suggestionState = simState\\n    -- 0.1s slack prevents timer from firing when there's not enough\\n    -- reaction time to matter\\n    if simState.gcdExpTime > time+0.1 then\\n        C_Timer.After(\\n            simState.gcdExpTime-time,\\n            function() WeakAuras.ScanEvents(\"SSR_UPDATE\") end\\n        )\\n    end\\n    \\n    local idxSug = 1\\n    while simState.time < aura_env.config.maxSimTime do\\n        local idxSugStart = idxSug\\n        local currSug = aura_env.func.suggest(simState)\\n        if currSug then\\n            if idxSug == 1 then\\n                currSug.anchorExpRel = 0\\n            else\\n                currSug.anchorExpRel = currSug.expirationTime-aura_env.suggestion[1].expirationTime\\n            end\\n            aura_env.suggestion[idxSug] = currSug\\n            if aura_env.config.log <= 0.5 then\\n                print(aura_env.func.fmtTime(time)..\":\"..idxSug..\\n                    \" sET=\"..aura_env.func.fmtTime(currSug.expirationTime - time)..\\n                    \" aETr=\"..aura_env.func.fmtTime(currSug.anchorExpRel)\\n                )\\n            end\\n            idxSug = idxSug+1\\n        end\\n        if aura_env.config.log <= 1.5 then print(\\n                \"update:\"..idxSugStart..\\n                \" t=\"..aura_env.func.fmtTime(simState.time)..\\n                \" E=\"..simState.energy..\\n                \" sug:\"..tostring(not not currSug)\\n        ) end\\n        simState = aura_env.func.updateState(simState, currSug)\\n        if not simState then break end\\n        if aura_env.config.log <= 1.5 then print(\\n                \"  post:\"..idxSugStart..\\n                \" t=\"..aura_env.func.fmtTime(simState.time)..\\n                \" E=\"..simState.energy\\n        ) end\\n    end\\n    \\n    for k in pairs(allstates) do allstates[k].show = false end\\n    for k in pairs(aura_env.suggestion) do\\n        -- PERF: Deep compare suggestions and leave standing ones alone?\\n        allstates[k] = aura_env.suggestion[k]\\n        if aura_env.config.log <= 2 then\\n            print(\\n                \"TSU: \"..k..\\n                \" name=\"..allstates[k].name..\\n                \" ET=\"..aura_env.func.fmtTime(allstates[k].expirationTime - time)..\\n                \" aETr=\"..aura_env.func.fmtTime(allstates[k].anchorExpRel)..\\n                \" dur=\"..aura_env.func.fmtTime(allstates[k].duration)\\n            )\\n        end\\n    end\\n    if aura_env.config.log <= 2 then print(\"==========Main End==========\") end\\n    return true\\nend",
            ["customVariables"] = "{ name = \"string\", remaining = \"number\", spellID = \"number\" }",
            ["custom_hide"] = "timed",
            ["custom_type"] = "stateupdate",
            ["debuffType"] = "HELPFUL",
            ["duration"] = "1",
            ["event"] = "Chat Message",
            ["events"] = "SSR_UPDATE,UNIT_AURA:player,UNIT_POWER_UPDATE:player,UNIT_SPELLCAST_STOP,UNIT_TARGET:player",
            ["names"] = {
            },
            ["spellIds"] = {
            },
            ["subeventPrefix"] = "SPELL",
            ["subeventSuffix"] = "_CAST_START",
            ["type"] = "custom",
            ["unevent"] = "timed",
            ["unit"] = "player",
          },
          ["untrigger"] = {
          },
        }, -- [1]
        {
          ["trigger"] = {
            ["check"] = "event",
            ["custom"] = "function(allstates, event, unit, powerType)\\n    if (\\n        GetShapeshiftFormID() == CAT_FORM and\\n        event == \"UNIT_POWER_UPDATE\" and\\n        powerType == \"ENERGY\"\\n    ) then\\n        local time = GetTime()\\n        local currEnergy = UnitPower(\"player\", 3)\\n        local energyDiff = currEnergy-(aura_env.etick.energy or 0)\\n        local timeDiff = time-(aura_env.etick.time or 0)\\n        if (\\n            event == \"UNIT_POWER_UPDATE\" and\\n            energyDiff >= 19 and\\n            energyDiff <= 23 and\\n            timeDiff > 1.9\\n        ) then\\n            aura_env.etick.time = time\\n            WeakAuras.ScanEvents(\"SSR_ENERGYTICK\", time)\\n            if aura_env.config.log <= 1 then print(\"Update energy tick time=\"..aura_env.func.fmtTime(aura_env.etick.time)) end\\n        end\\n        aura_env.etick.energy = currEnergy\\n        if aura_env.config.log <= 1 then print(\"Update energy tick energy=\"..aura_env.etick.energy) end\\n    end\\nend",
            ["custom_hide"] = "timed",
            ["custom_type"] = "stateupdate",
            ["debuffType"] = "HELPFUL",
            ["events"] = "UNIT_POWER_UPDATE:player",
            ["type"] = "custom",
            ["unit"] = "player",
          },
          ["untrigger"] = {
          },
        }, -- [2]
        {
          ["trigger"] = {
            ["check"] = "event",
            ["custom"] = "function(allstates, event, time)\\n    if event == \"SSR_ENERGYTICK\" and time == aura_env.etick.time then\\n        C_Timer.After(2, function() WeakAuras.ScanEvents(\"SSR_ENERGYTICK\", time) end)\\n        if aura_env.config.log <= 1 then print(\"TICK t=\"..aura_env.func.fmtTime(time)) end\\n    else\\n        if aura_env.config.log <= 1 then print(\"Stop energy tick chain \"..aura_env.func.fmtTime(time)..\" for \"..aura_env.func.fmtTime(aura_env.etick.time)) end\\n    end\\nend",
            ["custom_hide"] = "timed",
            ["custom_type"] = "stateupdate",
            ["debuffType"] = "HELPFUL",
            ["events"] = "SSR_ENERGYTICK",
            ["type"] = "custom",
            ["unit"] = "player",
          },
          ["untrigger"] = {
          },
        }, -- [3]
        {
          ["trigger"] = {
            ["debuffType"] = "HELPFUL",
            ["event"] = "Stance/Form/Aura",
            ["form"] = {
              ["single"] = 3,
            },
            ["type"] = "unit",
            ["unit"] = "player",
            ["use_form"] = true,
            ["use_unit"] = true,
          },
          ["untrigger"] = {
          },
        }, -- [4]
      },
      ["uid"] = "B)nMZp74qKL",
      ["url"] = "https://wago.io/chlTNgi3I/13",
      ["version"] = 13,
      ["wagoID"] = "chlTNgi3I",
      ["width"] = 32,
      ["xOffset"] = 0,
      ["yOffset"] = 0,
      ["zoom"] = 0.3,
    },
  },
  ["name"] = "Shred Shred Revolution",
  ["parent"] = {
    ["actions"] = {
      ["finish"] = {
      },
      ["init"] = {
      },
      ["start"] = {
      },
    },
    ["anchorFrameType"] = "SCREEN",
    ["anchorPoint"] = "CENTER",
    ["animation"] = {
      ["finish"] = {
        ["duration_type"] = "seconds",
        ["easeStrength"] = 3,
        ["easeType"] = "none",
        ["type"] = "none",
      },
      ["main"] = {
        ["duration_type"] = "seconds",
        ["easeStrength"] = 3,
        ["easeType"] = "none",
        ["type"] = "none",
      },
      ["start"] = {
        ["duration_type"] = "seconds",
        ["easeStrength"] = 3,
        ["easeType"] = "none",
        ["type"] = "none",
      },
    },
    ["authorOptions"] = {
    },
    ["backdropColor"] = {
      1, -- [1]
      1, -- [2]
      1, -- [3]
      0.5, -- [4]
    },
    ["border"] = false,
    ["borderBackdrop"] = "Blizzard Tooltip",
    ["borderColor"] = {
      0, -- [1]
      0, -- [2]
      0, -- [3]
      1, -- [4]
    },
    ["borderEdge"] = "Square Full White",
    ["borderInset"] = 1,
    ["borderOffset"] = 4,
    ["borderSize"] = 2,
    ["conditions"] = {
    },
    ["config"] = {
    },
    ["controlledChildren"] = {
      "SSR - Bar", -- [1]
      "SSR - Main", -- [2]
      "SSR - Background", -- [3]
      "SSR - GCD", -- [4]
      "SSR - ETick 1", -- [5]
      "SSR - ETick 2", -- [6]
      "SSR - ETick 3", -- [7]
    },
    ["frameStrata"] = 1,
    ["id"] = "Shred Shred Revolution",
    ["information"] = {
    },
    ["internalVersion"] = 45,
    ["load"] = {
      ["class"] = {
        ["multi"] = {
        },
      },
      ["size"] = {
        ["multi"] = {
        },
      },
      ["spec"] = {
        ["multi"] = {
        },
      },
      ["talent"] = {
        ["multi"] = {
        },
      },
    },
    ["regionType"] = "group",
    ["scale"] = 1,
    ["selfPoint"] = "CENTER",
    ["subRegions"] = {
    },
    ["triggers"] = {
      {
        ["trigger"] = {
          ["debuffType"] = "HELPFUL",
          ["event"] = "Health",
          ["names"] = {
          },
          ["spellIds"] = {
          },
          ["subeventPrefix"] = "SPELL",
          ["subeventSuffix"] = "_CAST_START",
          ["type"] = "aura2",
          ["unit"] = "player",
        },
        ["untrigger"] = {
        },
      }, -- [1]
    },
    ["uid"] = "JlIxpDTMx2I",
    ["xOffset"] = -0.00341796875,
    ["yOffset"] = -135,
  },
}