# Roadmap

* Simulation
  * [x] State tracked
    * [x] GCD
    * [x] Mangle debuff
    * [x] Rip debuff
    * [x] Clearcasting buff
    * [x] Energy
  * [x] State effects
    * [x] GCD
    * [x] Ability energy cost
    * [x] Ability changes (de)buff state
    * [x] Energy ticks
  * [x] Suggest next ability to use
  * [x] Simulate multiple suggestions
  * [ ] Include abilities that ignore GCD
* Style
  * [x] Animate icon slide to bottom
  * [x] Each icon is positioned relative to how long until it's cast
  * [x] Each cast slides towards time zero
  * [x] Each cast pauses when casts before it delay
  * [ ] Each spell has a column with multiple casts
  * [ ] Tick or line at each GCD position
  * [ ] Desaturated, low alpha icon of spell at bottom of bar
  * [ ] Bar background to give relative scale and endpoint
  * [ ] Color on bar section for cast queue time (default=400ms)
    * [ ] Add queue time config option
    * [ ] Read queue time from CVar
* DevOps
  * [ ] Manually deploy WeakAura to wago.io
  * [ ] Construct WeakAura JSON structure from code snippets and metadata
  * [ ] Minify Lua
  * [ ] Automatically deploy WeakAura to wago.io

## Wishlist

* Classes/Roles
  * [ ] Druid - Feral Cat
  * [ ] Druid - Feral Bear
  * [ ] Shaman - Elemental
* Config-defined rotation
* Empower player decision-making
  * Vertical energy ticks
  * Vertical GCD progress bar (single)
* Smooth out big suggestion changes
  * Causes
    * Energy ticks
    * Crits/Misses
      * Show Bayesian suggestions with a tree rather than an array
      * Crit path in red
      * Miss path in blue
      * Alpha for probability
  * Fade in suggestions if player waits for energy tick
    * Smooths out jumpiness of big suggestion changes
    * Make sure to ignore case where tick isn't correctly detected
    * Might be worth fading out current suggestions too
* Player feedback
  * Grow animation for icon when correct ability used
  * Timing scoring?
* Event driven architecture
* Use queue bar as indicator for whether the spell can be cast now
  * Style
    * Red = missing resource
    * Grey = cast not in queue window (maybe)
    * Green = yes
  * Implementation
    * Query and cache spell costs
    * Emit SSR_CAST event (in icon WA) after icon state updates with cost(s)
    * Add SSR_CAST event trigger to queue bar that checks whether current resources are sufficient and conditional that colors the bar
