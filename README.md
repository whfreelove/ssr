# Shred Shred Revolution

This "Shred Shred Revolution" World of Warcraft WeakAura helps
players learn ability rotations through a timing-game-based visual style minigame.

![Example](docs/example.png)

- [Shred Shred Revolution](#shred-shred-revolution)
  - [Sources](#sources)
  - [Rotation](#rotation)
  - [Architecture](#architecture)
    - [Actions](#actions)
      - [`init.custom`](#initcustom)
    - [Triggers](#triggers)
      - [Update Trigger](#update-trigger)
    - [Animation](#animation)
      - [`main.translateFunc`](#maintranslatefunc)

## Sources

- Rotation: [Druid Discord - #tbc-feral-dps](https://discord.gg/ZEpbaBh5cY)
- Energy Ticks: [Energy Tick Bar Classic WeakAura](https://wago.io/L1eTpdV2Q)
- Sliding Icons: [CoolLine WeakAura](https://wago.io/chlTNgi3I)
- [WeakAuras Documentation](https://github.com/WeakAuras/WeakAuras2/wiki)
- [WoW API](https://wowpedia.fandom.com/wiki/World_of_Warcraft_API)
  - [Events](https://wowpedia.fandom.com/wiki/Events)

## Rotation

![Flowchart](docs/flowchart_20220104.png)

## Architecture

```mermaid
graph LR

ACTION(Action Init)
ACTION --> SSR_UPDATE

T1(Trigger Main)
SSR_UPDATE -.-> T1
UNIT_AURA:player -.-> T1
UNIT_AURA:target -.-> T1
UNIT_POWER_UPDATE:player -.-> T1
UNIT_SPELLCAST_STOP:player -.-> T1
UNIT_TARGET:player -.-> T1

T1 --> TSU

TSU --> Conditions
Conditions --> Animation
```

### Actions

#### `init.custom`

Define `aura_env` values, tables, arrays, and functions then emit `SSR_UPDATE`

- Sends
  - `SSR_UPDATE` Consumed by trigger
- Local Functions
- `aura_env.func`: repository of functions for use across triggers and animations
  - `cooldownExpTime`: query a cooldown from WoW API, clean it, and return an `expirationTime` equivalent
  - `dumpTable`: build a string from a table enumerating its contents for debug
  - `fmtTime`: provide formatted times for debug
  - `searchAura`: query WoW API to determine whether a (de)buff exists on the unit
    and get its expiration time
  - `stateMatch`: shallow comparison of stored state table and one passed in
  - `getState`: query WoW API for state relative to rotation and return as table
  - `suggest`: using state, determine what spell to use from a rotation definition
    - `spell`: enabled deep copy of spellInfo by ID
    - `spellInfo`: query WoW API for name and icon from a spell ID and return as table
  - `updateState`: determine what the next simulation time point should be and update
    state to match its changes

[Code](action.lua)

### Triggers

#### Update Trigger

- Trigger On
  - `SSR_UPDATE`
  - `UNIT_AURA:player`
  - `UNIT_AURA:target`
  - `UNIT_POWER_UPDATE:player`
  - `UNIT_SPELLCAST_STOP:player`
  - `UNIT_TARGET:player`

NOTE: Put in Trigger 1 for simplicity

1. Take existing suggestions and hold as `prevSug`
2. If previous suggestion exists and the last time a suggestion was made was also
   in this frame, make no updates
3. Query and save current state information
4. If state information is the same as when the last suggestion was made, make
   no updates
5. Begin simulation loop (run until beyond max simulation time)
   1. Get a possible suggestion and assign it a time
   2. If current suggestion is to wait (from rotation flow or because CDs prevent
      action), skip updating suggestions
   3. Otherwise suggestion is a spell, so update the suggestions (and pause?)
   4. For non-first suggestions, link their parent for animation limits
   5. Update state based on what suggestion was selected
6. Update TSU `allstates` with the suggestions

[Code](trigger.lua)

### Animation

#### `main.translateFunc`

1. If state information isn't accessible, do nothing
2. Set progress to the time remaining (expiration - now) over the total time the
   bar represents (max suggestion time)
   1. Do not allow the value to become negative (stop at bottom of bar)
   2. Do not allow the value to become less than the parent's time + GCD
3. Scale the progress relative to the total simulation time
4. Project relative progress onto the size of the bar

[Code](animation.lua)
