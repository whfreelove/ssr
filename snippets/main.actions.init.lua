if aura_env.config.log <= 2 then
    print("")
    print("============Init============")
end
aura_env.barName = "SSR - Queue"
aura_env.barHeight = WeakAuras.GetRegion(aura_env.barName).height
aura_env.etick = {}
aura_env.suggestion = {}
aura_env.suggestionState = {}

local function cooldownExpTime(spellID)
    local start,duration,_,_ = GetSpellCooldown(spellID)
    if start == 0 then
        return
    else
        return start + duration
    end
end

local function dumpTable(tbl, fmt)
    if not fmt then fmt = tostring end
    local res = ""
    for k,v in pairs(tbl) do
        res = res.." "..k.."="..fmt(v)
    end
    return res
end

local function fmtTime(t)
    if not t then return 0 else return math.floor(t%60*100)/100 end
end

local function searchAura(unit, filter, sourcePlayer, searchIDs)
    for i=1,40 do
        local _,_,_,_,_,expTime,source,_,_,spellID = UnitAura(unit, i, filter)
        if not spellID or (sourcePlayer and source ~= "player") then
            break
        end
        for _,searchID in ipairs(searchIDs) do
            if aura_env.config.log <= 0.5 then
                print(
                    unit..
                    " spell="..spellID..
                    "|search="..searchID..
                    " source="..(source or "nil")
                )
            end
            if spellID == searchID then
                return true, expTime
            end
        end
    end
    return false, GetTime()-1
end

local function stateMatch(simState)
    for k in pairs(simState) do
        if simState[k] ~= aura_env.suggestionState[k] then
            return false
        end
    end
    return true
end

local function getState()
    local time = GetTime()
    local autoattack = IsCurrentSpell(6603)
    local energy = UnitPower("player", 3)
    local comboPts = GetComboPoints("player", "target")
    local mana = UnitPower("player", 0)
    local faerieCDExpTime = cooldownExpTime(27011)
    local gcdExpTime = cooldownExpTime(29515)
    local cc, ccExpTime = searchAura("player", "HELPFUL", false, {16870})
    local faerie, faerieExpTime = searchAura("target", "HARMFUL", false, {26993, 27011})
    local mangle, mangleExpTime = searchAura("target", "HARMFUL", false, {33983, 33987})
    local rake, rakeExpTime = searchAura("target", "HARMFUL", true, {27003})
    local rip, ripExpTime = searchAura("target", "HARMFUL", true, {27008})
    local tf, tfExpTime = searchAura("player", "HELPFUL", true, {9846})
    local simState = {
        time = 0,
        autoattack = autoattack,
        energy = energy,
        comboPts = comboPts,
        mana = mana,
        gcd = not not gcdExpTime,
        cc = cc,
        faerie = faerie,
        mangle = mangle,
        rake = rake,
        rip = rip,
        tf = tf,
        etickExpTime = ((aura_env.etick.time or 0)-time)%2+time,
        faerieCDExpTime = faerieCDExpTime or time-1,
        gcdExpTime = gcdExpTime or time-1,
        ccExpTime = ccExpTime,
        faerieExpTime = faerieExpTime,
        mangleExpTime = mangleExpTime,
        rakeExpTime = rakeExpTime,
        ripExpTime = ripExpTime,
        tfExpTime = tfExpTime,
    }
    if aura_env.config.log <= 2 then print("simState: "..aura_env.func.dumpTable(simState)) end
    return simState
end

local function suggest(simState)
    local time = GetTime()
    local etickWait = simState.etickExpTime-time-simState.time
    local gcdWait = simState.gcdExpTime-time-simState.time
    if etickWait < 0.2 then
        if aura_env.config.log <= 1 then print("Too close to tick: "..aura_env.func.fmtTime(etickWait)) end
        return nil
    elseif gcdWait > 0 then
        if aura_env.config.log <= 1 then print("GCD up: "..aura_env.func.fmtTime(simState.gcdExpTime - time)) end
        return nil
    end
    
    local spellInfo = function(spellID)
        local name,_,icon,_,_,_ = GetSpellInfo(spellID)
        return  {
            spellID = spellID,
            name = name,
            icon = icon,
        }
    end
    
    local spellInfoCache = {
        [768] = spellInfo(768),  -- Cat Form
        [9846] = spellInfo(9846),  -- Tiger's Fury
        [24248] = spellInfo(24248),  -- Ferocious Bite
        [27002] = spellInfo(27002),  -- Shred
        [27003] = spellInfo(27003),  -- Rake
        [27008] = spellInfo(27008),  -- Rip
        [27011] = spellInfo(27011),  -- Faerie Fire (Feral)
        [33983] = spellInfo(33983),  -- Mangle (Cat)
    }
    
    local spellNameCache = {
        [768] = "Shift",
        [9846] = "Tiger's Fury",
        [24248] = "Bite",
        [27002] = "Shred",
        [27003] = "Rake",
        [27008] = "Rip",
        [27011] = "Faerie Fire",
        [33983] = "Mangle",
    }
    
    -- Shallow copy of spell info so suggestions are independent
    local function spell(spellID)
        local res = {}
        for k,v in pairs(spellInfoCache[spellID]) do res[k] = v end
        if aura_env.config.log <= 1 then print("Suggest: "..spellNameCache[spellID]) end
        return res
    end
    
    -- Rotation
    local etickSoon = etickWait < 1
    if simState.cc then
        if simState.mangle then
            if etickSoon and simState.energy < 22 then
                suggest = spell(768)  -- Shift
            else
                suggest = spell(27002)  -- Shred
            end
        elseif (etickSoon and simState.energy >= 20 and simState.energy < 40) or (not etickSoon and simState.energy >= 40 and simState.energy < 42) then
            suggest = spell(27002)  -- Shred
        else
            suggest = spell(33983)  -- Mangle
        end
    elseif simState.comboPts >= 4 and not simState.rip then
        if simState.energy >= 30 then
            if simState.energy >= 80 and not simState.tf then
                suggest = spell(9846)  -- Tiger's Fury
            else
                suggest = spell(27008)  -- Rip
            end
        else
            if not etickSoon or simState.energy < 10 then
                suggest = spell(768)  -- Shift
            else
                if aura_env.config.log <= 1 then print("Wait") end
                return nil
            end
        end
    elseif simState.comboPts == 5 then
        if simState.energy >= 57 then
            suggest = spell(27002)  -- Shred
        elseif simState.energy >= 35 then
            suggest = spell(24248)  -- Bite
        elseif not etickSoon or simState.energy < 15 then
            suggest = spell(768)  -- Shift
        else
            if aura_env.config.log <= 1 then print("Wait") end
            return nil
        end
    else
        if simState.mangle and simState.energy >= 42 then
            suggest = spell(27002)  -- Shred
        elseif simState.energy >= 40 then
            suggest = spell(33983)  -- Mangle
        elseif etickSoon then
            if simState.energy < 20 then
                suggest = spell(768)  -- Shift
            else
                if aura_env.config.log <= 1 then print("Wait") end
                return nil
            end
        elseif simState.energy < 35 then
            suggest = spell(768)  -- Shift
        elseif simState.comboPts >= 2 then
            suggest = spell(24248)  -- Bite
        else
            suggest = spell(27003)  -- Rake
        end
    end
    
    suggest.autoHide = false
    suggest.changed = true
    suggest.duration = simState.time
    suggest.expirationTime = time+simState.time
    suggest.paused = false
    suggest.progressType = "timed"
    suggest.show = true
    return suggest
end

local updateState = function(simState, suggestion)
    local time = GetTime()
    local remaining = {
        [0] = simState.etickExpTime-time,
        [9846] = simState.tfExpTime-time,
        [16870] = simState.ccExpTime-time,
        [27003] = simState.rakeExpTime-time,
        [27008] = simState.ripExpTime-time,
        [27011] = simState.faerieExpTime-time,
        [29515] = simState.gcdExpTime-time,
        [33983] = simState.mangleExpTime-time,
    }
    if suggestion then
        local gcdUpdate
        if suggestion.spellID == 768 then
            gcdUpdate = 1.5
        elseif suggestion.spellID == 9846 then
            gcdUpdate = 0
        else
            gcdUpdate = 1
        end
        simState.gcdExpTime = time+simState.time+gcdUpdate
        remaining[29515] = simState.time+gcdUpdate
    end
    if aura_env.config.log <= 0.5 then print("  Remain:"..dumpTable(remaining, aura_env.func.fmtTime)) end
    local updateCause = {
        -- Energy Tick [Fake ID]
        [0] = function(simState)
            simState.energy = simState.energy+20
            simState.etickExpTime = simState.etickExpTime+2
            return simState
        end,
        -- Tiger's Fury
        [9846] = function(simState)
            simState.tf = false
            simState.tfExpTime = GetTime()-1
            return simState
        end,
        -- Clearcasting [Omen of Clarity]
        [16870] = function(simState)
            simState.cc = false
            simState.ccExpTime = GetTime()-1
            return simState
        end,
        -- Rake
        [27003] = function(simState)
            simState.rake = false
            simState.rakeExpTime = GetTime()-1
            return simState
        end,
        -- Rip
        [27008] = function(simState)
            simState.rip = false
            simState.ripExpTime = GetTime()-1
            return simState
        end,
        -- Faerie Fire (Feral)
        [27011] = function(simState)
            simState.faerie = false
            simState.faerieExpTime = GetTime()-1
            return simState
        end,
        -- GCD
        [29515] = function(simState)
            simState.gcdExpTime = GetTime()-1
            return simState
        end,
        -- Mangle (Cat)
        [33983] = function(simState)
            simState.mangle = false
            simState.mangleExpTime = GetTime()-1
            return simState
        end,
    }
    local spellEffect = {
        -- Powershift
        [768] = function(simState)
            -- TODO: Check for Wolfshead Helm
            simState.energy = 60
            return simState
        end,
        -- Tiger's Fury
        [9846] = function(simState)
            if simState.cc then
                simState.cc = false
            else
                simState.energy = simState.energy-30
            end
            simState.tf = true
            simState.tfExpTime = time+simState.time+6
            return simState
        end,
        -- Ferocious Bite
        [24248] = function(simState)
            if simState.cc then
                simState.cc = false
            end
            simState.energy = 0
            simState.comboPts = 0
            return simState
        end,
        -- Shred
        [27002] = function(simState)
            -- TODO: Check for talents and set bonuses?
            if simState.cc then
                simState.cc = false
            else
                simState.energy = simState.energy-42
            end
            simState.comboPts = simState.comboPts+1
            return simState
        end,
        -- Rake
        [27003] = function(simState)
            if simState.cc then
                simState.cc = false
            else
                simState.energy = simState.energy-35
            end
            simState.comboPts = simState.comboPts+1
            simState.rake = true
            simState.rakeExpTime = time+simState.time+9
            return simState
        end,
        -- Rip
        [27008] = function(simState)
            if simState.cc then
                simState.cc = false
            else
                simState.energy = simState.energy-30
            end
            simState.comboPts = 0
            simState.rip = true
            simState.ripExpTime = time+simState.time+12
            return simState
        end,
        -- Faerie Fire (Feral)
        [27011] = function(simState)
            simState.faerie = true
            simState.faerieExpTime = time+simState.time+40
            simState.faerieCDExpTime = time+simState.time+6
            return simState
        end,
        -- Mangle (Cat)
        [33983] = function(simState)
            -- TODO: Check for talents and set bonuses?
            if simState.cc then
                simState.cc = false
            else
                simState.energy = simState.energy-40
            end
            simState.comboPts = simState.comboPts+1
            simState.mangle = true
            simState.mangleExpTime = time+simState.time+12
            return simState
        end,
    }
    local idxNext
    local simTime = aura_env.config.maxSimTime
    for k,v in pairs(remaining) do
        if v >= simState.time and v < simTime then
            idxNext, simTime = k,v
        end
    end
    if not idxNext then
        if aura_env.config.log <= 1 then print("Simulation time limit is "..aura_env.func.fmtTime(aura_env.config.maxSimTime)) end
        return
    end
    if aura_env.config.log <= 1 then print("  r="..aura_env.func.fmtTime(simTime).." from "..idxNext) end
    simState = updateCause[idxNext](simState)
    if suggestion and spellEffect[suggestion.spellID] then
        simState = spellEffect[suggestion.spellID](simState)
    end
    simState.time = simTime
    return simState
end

aura_env.func = {
    dumpTable = dumpTable,
    fmtTime = fmtTime,
    getState = getState,
    stateMatch = stateMatch,
    suggest = suggest,
    updateState = updateState,
}

WeakAuras.ScanEvents ('SSR_UPDATE')
if aura_env.config.log <= 2 then print("==========Init End==========") end
