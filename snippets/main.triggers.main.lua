function(allstates, event)
    if GetShapeshiftFormID() ~= CAT_FORM or not aura_env.func then return false end
    if aura_env.config.log <= 2 then
        print("")
        print("============Main============")
        print("Trigger: "..event)
    end
    local time = GetTime()
    local prevSug = next(aura_env.suggestion)
    -- If a previous suggestion already exists and either this
    --     is in the same frame, do not update
    if not not prevSug and aura_env.suggestion.time == time then
        return false
    end

    local simState = aura_env.func.getState()
    -- If the state is unchanged, do not update
    if not not prevSug and aura_env.func.stateMatch(simState) then
        if aura_env.config.log <= 1 then print("-- State match t="..aura_env.func.fmtTime(time)) end
        return false
    else  -- Since the state is new, update the stored state
        aura_env.suggestionState = simState
    end

    -- 0.25s slack prevents timer from firing when there's not enough
    -- reaction time to matter
    local gcdWait = simState.gcdExpTime-time
    if gcdWait > 0.75 then  -- An ability was just used and state is likely inaccurate
        if aura_env.config.log <= 1 then print("-- Just used ability t="..aura_env.func.fmtTime(time)) end
        C_Timer.After(
            gcdWait-0.75,
            function() WeakAuras.ScanEvents("SSR_UPDATE") end
        )
        return false
    elseif gcdWait > 0.25 then
        C_Timer.After(
            gcdWait,
            function() WeakAuras.ScanEvents("SSR_UPDATE") end
        )
    end

    local idxSug = 1
    while simState.time < aura_env.config.maxSimTime do
        local idxSugStart = idxSug
        local currSug = aura_env.func.suggest(simState)
        if currSug then
            if idxSug == 1 then
                currSug.anchorExpRel = 0
            else
                currSug.anchorExpRel = currSug.expirationTime-aura_env.suggestion[1].expirationTime
            end
            aura_env.suggestion[idxSug] = currSug
            if aura_env.config.log <= 0.5 then
                print(aura_env.func.fmtTime(time)..":"..idxSug..
                    " sET="..aura_env.func.fmtTime(currSug.expirationTime-time)..
                    " aETr="..aura_env.func.fmtTime(currSug.anchorExpRel)
                )
            end
            idxSug = idxSug+1
        end
        if aura_env.config.log <= 1.5 then print(
            "  update:"..idxSugStart..
            " t="..aura_env.func.fmtTime(simState.time)..
            " E="..simState.energy..
            " sug:"..tostring(not not currSug)
        ) end
        simState = aura_env.func.updateState(simState, currSug)
        if not simState then break end
        if aura_env.config.log <= 1.5 then print(
            "  post:"..idxSugStart..
            " t="..aura_env.func.fmtTime(simState.time)..
            " E="..simState.energy
        ) end
    end

    for k in pairs(allstates) do allstates[k].show = false end
    for k in pairs(aura_env.suggestion) do
        -- PERF: Deep compare suggestions and leave standing ones alone?
        allstates[k] = aura_env.suggestion[k]
        if aura_env.config.log <= 2 and allstates[k].show then
            print(
                "TSU: "..k..
                " name="..allstates[k].name..
                " ET="..aura_env.func.fmtTime(allstates[k].expirationTime - time)..
                " aETr="..aura_env.func.fmtTime(allstates[k].anchorExpRel)..
                " dur="..aura_env.func.fmtTime(allstates[k].duration)
            )
        end
    end
    if aura_env.config.log <= 2 then print("==========Main End==========") end
    return true
end
