function(allstates, event, unit, powerType)
    if (
        GetShapeshiftFormID() == CAT_FORM and
        event == "UNIT_POWER_UPDATE" and
        powerType == "ENERGY"
    ) then
        local time = GetTime()
        local currEnergy = UnitPower("player", 3)
        local energyDiff = currEnergy-(aura_env.etick.energy or 0)
        local timeDiff = time-(aura_env.etick.time or 0)
        if (
            event == "UNIT_POWER_UPDATE" and
            energyDiff >= 19 and
            energyDiff <= 23 and
            timeDiff > 1.9
        ) then
            aura_env.etick.time = time
            WeakAuras.ScanEvents("SSR_ENERGYTICK", time)
            if aura_env.config.log <= 1 then print("Update energy tick time="..aura_env.func.fmtTime(aura_env.etick.time)) end
        end
        aura_env.etick.energy = currEnergy
        if aura_env.config.log <= 1 then print("Update energy tick energy="..aura_env.etick.energy) end
    end
end
