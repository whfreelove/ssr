function(allstates, event, time)
    if event == "SSR_ENERGYTICK" and time == aura_env.etick.time then
        C_Timer.After(2, function() WeakAuras.ScanEvents("SSR_ENERGYTICK", time) end)
        if aura_env.config.log <= 1 then print("TICK t="..aura_env.func.fmtTime(time)) end
    else
        if aura_env.config.log <= 1 then print("Stop energy tick chain "..aura_env.func.fmtTime(time).." for "..aura_env.func.fmtTime(aura_env.etick.time)) end
    end
end
