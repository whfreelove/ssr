function(allstates, event, time)
    if event == "SSR_ENERGYTICK" then
        allstates[""] = {
            changed = true,
            duration = 2,
            expirationTime = GetTime()+2,
            progressType = "timed",
            show = true
        }
        return true
    end
end
