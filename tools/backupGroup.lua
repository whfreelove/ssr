if #arg ~= 4 then print("Incorrect arguments"); return end
local path = arg[1]
local version = arg[2]
local account = arg[3]
local groupName = arg[4]

local printTableRequire = require("printTable")
package.path = package.path..";"..os.getenv("ProgramFiles(x86)").."\\World of Warcraft\\_"..version.."_\\WTF\\Account\\"..account.."\\SavedVariables\\WeakAuras.lua"
local weakAurasRequire = require("WeakAuras") or print("WeakAuras.lua load failed")

local group = {
    children = {},
    name = groupName,
    parent = WeakAurasSaved.displays[groupName],
}
for _,v in ipairs(group.parent.controlledChildren) do
    group.children[v] = WeakAurasSaved.displays[v]
end

printTable(group, groupName..".lua")
