--[[
http://lua-users.org/wiki/SortedIteration
Ordered table iterator, allow to iterate on the natural order of the keys of a
table.
]]

--[[
printTableHelper
https://gist.github.com/marcotrosi/163b9e890e012c6a460a
A simple function to print tables or to write tables into files.
Great for debugging but also for data storage.
When writing into files the 'return' keyword will be added automatically,
so the tables can be loaded with 'dofile()' into a variable.
The basic datatypes table, string, number, boolean and nil are supported.
The tables can be nested and have number and string indices.
This function has no protection when writing files without proper permissions and
when datatypes other then the supported ones are used.
]]

function printTable(tbl, fn)
    local file = io.open(fn, "w")

    local function __genOrderedIndex(t)
        local orderedIndexNum = {}
        local orderedIndexStr = {}
        for k in pairs(t) do
            if type(k) == "number" then
                table.insert(orderedIndexNum, k)
            elseif type(k) == "string" then
                table.insert(orderedIndexStr, k)
            end
        end
        table.sort(orderedIndexNum)
        table.sort(orderedIndexStr)
        local orderedIndex = {table.unpack(orderedIndexNum)}
        table.move(
            orderedIndexStr,
            1,
            #orderedIndexStr,
            #orderedIndex+1,
            orderedIndex
        )
        return orderedIndex
    end

    local function orderedNext(t, state)
        local k = nil
        if not state then
            t.__orderedIndex = __genOrderedIndex(t) 
            k = t.__orderedIndex[1]
        else
            for i = 1,#t.__orderedIndex do
                if t.__orderedIndex[i] == state then
                    k = t.__orderedIndex[i+1]
                end
            end
        end

        if k then
            return k, t[k]
        end

        t.__orderedIndex = nil
        return
    end

    local function orderedPairs(t)
        return orderedNext, t, nil
    end

    local function isArray(tbl)
        if #tbl == 0 then return false end
        for i,_ in ipairs(tbl) do
            if not tbl[i] then return false end
        end
        return true
    end

    local function printTableHelper(obj, cnt, assign)
        local cnt = cnt or 0
        if type(obj) == "table" then
            if not assign then
                file:write(string.rep("  ", cnt))
            end
            file:write("{\n")
            cnt = cnt+1
            if isArray(obj) then
                for i,v in ipairs(obj) do
                    if type(v) == "string" then
                        file:write(string.rep("  ", cnt), '"', v, '"')
                    elseif type(v) == "number" then
                        file:write(string.rep("  ", cnt), v)
                    else
                        printTableHelper(v, cnt, false)
                    end
                    file:write(", -- [", i, "]\n")
                end
            else
                for k,v in orderedPairs(obj) do
                    if type(k) == "string" then
                        file:write(string.rep("  ", cnt), '["', k, '"]', " = ")
                    elseif type(k) == "number" then
                        file:write(string.rep("  ", cnt), "[", k, "]", " = ")
                    end
                    printTableHelper(v, cnt, true)
                    file:write(",\n")
                end
            end
            cnt = cnt-1
            file:write(string.rep("  ", cnt), "}")
        elseif type(obj) == "string" then
            file:write(string.format("%q", obj:gsub("\n", "\\n")))
        else
            file:write(tostring(obj))
        end
        file:flush()
    end

    printTableHelper(tbl)
    file:close()
end
